<?LassoScript


// ----------------------------------------------------------------------------------------------------------
// [POPGet -> (BodyPart)]
// 
// ----------------------------------------------------------------------------------------------------------


Define_Tag: 'BodyPart';
		
		local: 'PM_pgID' 		 	   = (self -> 'PM_pID');
		local: 'PM_pContentType'	   = (named_param: '-contenttype');
		local: 'PM_pgHeaders'		   = (self->'PM_pHeaders');
		local: 'PM_pgAryMessage' 	   = (self->'PM_aryMessage');
		local: 'PM_pgMessageBodyParts' = (self->'PM_messageBodyParts');
		local: 'PM_pgAllBodyParts'	   = (self->'PM_pAllBodyParts');
		local: 'PM_rFinal'			   = array;
		local: 'PM_tLoopNo'			   = 0;
		local: 'PM_msgContentType'	   = string;
		local: 'PM_tCurType'		   = string;
	
		if: ((self->'PM_connection')->'PM_errorMessage') == 'No error';
		
		library: $__POPmill_pgMi__;
	
		if: #PM_pgID != '';
			#PM_pID = 1;
		/if;
	
	
		if: #PM_pgID == '';
			Fail_If: #PM_pID == '', 3, 'The -id or -uid parameters were not used or defined in [POPGet -> (BodyPart)]';
			Fail_If: #PM_pContentType == '', 3, 'The -ContentType parameter was not used or defined in [POPGet -> (BodyPart)]';
		else: #PM_pgID != '';
			Fail_If: #PM_pContentType == '', 3, 'The -ContentType parameter was not used or defined in [POPGet -> (BodyPart)]';
		/if;
						

		
					
			if: #PM_pID != 'false';
				
				if: #PM_pgAllBodyParts == false;
					
					if: #PM_pgAryMessage->size >= #PM_pID;
						#PM_rFinal = (Parse_Body: -message=(#PM_pgAryMessage->get:#PM_pID->find:'raw'), -type=#PM_pContentType);
					/if;
					
				else;
																						 
					if: #PM_pgMessageBodyParts->size != 0;
					
						Loop: #PM_pgMessageBodyParts->size;
			
							if: #PM_pgMessageBodyParts->get:loop_count->last == #PM_pID;
									
								#PM_tLoopNo = loop_count;
								
								if: #PM_pContentType->type == 'string';
								
									loop: #PM_pgMessageBodyParts->get:#PM_tLoopNo->size;
																										 
										if: #PM_pgMessageBodyParts->get:#PM_tLoopNo->get:loop_count->type != 'integer';
											#PM_msgContentType = #PM_pgMessageBodyParts->(get:#PM_tLoopNo)->(get:loop_count)->(find: 'Content-Type')->(split: ';')->(get: 1);
										else: #PM_pgMessageBodyParts->get:#PM_tLoopNo->get:loop_count->type == 'integer';
											loop_abort;
										/if;
						
										if: #PM_msgContentType >> #PM_pContentType;
											#PM_rFinal->insert:(#PM_pgMessageBodyParts->(get:#PM_tLoopNo)->(get:loop_count));
										/if;

									/loop;
							
							
								else: #PM_pContentType->type == 'array';
								
									loop: #PM_pContentType->size;
								
										#PM_tCurType = #PM_pContentType->get:loop_count;
								
										loop: #PM_pgMessageBodyParts->get:#PM_tLoopNo->size;
																										 
											if: #PM_pgMessageBodyParts->get:#PM_tLoopNo->get:loop_count->type != 'integer';
												#PM_msgContentType = #PM_pgMessageBodyParts->(get:#PM_tLoopNo)->(get:loop_count)->(find: 'Content-Type')->(split: ';')->(get: 1);
											else: #PM_pgMessageBodyParts->get:#PM_tLoopNo->get:loop_count->type == 'integer';
												loop_abort;
											/if;
							
											if: #PM_msgContentType >> #PM_tCurType;
												#PM_rFinal ->insert: (#PM_pgMessageBodyParts->(get:#PM_tLoopNo)->(get:loop_count));
											/if;
								
										/loop;
									
									/loop;
									
								/if;
								
							/if;
						/Loop;
					/if;
				/if;
				
			
			/if;
				
			if: #PM_rFinal->size != 0;
		
				return: #PM_rFinal;
			else;
				return: array;
			/if;
						
	/if;
	
	/Define_Tag;

?>