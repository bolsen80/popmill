<?LassoScript

// ************************************************************************************
// [POPGet -> (AllMessageSizes)]
//
// Outputs the combined total of message SIZEs.
// ************************************************************************************
	
	Define_Tag: 'AllMessageSizes';
	
		local: 'PM_pNewMessages' 		= false;
		local: 'PM_PGNewMessages' 		= (self->'PM_pNewMessages');
		local: 'PM_aryDownloadSpecific' = (self->'PM_aryDownloadSpecific');
		local: 'PM_aryLIST' 			= (self->'PM_aryLIST');
		local: 'PM_tSize'				= 0;
		local: 'PM_tCurrentMsg'			= integer;
		local: 'PM_allMessageSizes'		= (self->'PM_allMessageSizes');
	
		 Loop: params->size;
			 if: params->get:loop_count == '-newMessages';
				#PM_pNewMessages = true;
				loop_abort;
			 /if;
	    /Loop;
	
		if: ((self->'PM_connection')->'PM_errorMessage') == 'No error';
			
				if: #PM_pNewMessages && #PM_PGNewMessages;
									
					loop: #PM_aryDownloadSpecific->size;
						
						#PM_tCurrentMsg = #PM_aryDownloadSpecific->get:loop_count;
						
						loop: #PM_aryLIST->size;
						
						
							if: #PM_aryLIST->get:loop_count->split:' '->get:1 == #PM_tCurrentMsg;
								#PM_tSize = (integer: #PM_tSize) + (integer: #PM_aryLIST->get:loop_count->get:2);
								loop_abort;
							/if;
						
						/loop;
						
					/loop;
					
					return: #PM_tSize;
				
				else;
					return: #PM_allMessageSizes;
				/if;
			
		else;
			return: 0;
			
		/if;
	
	/Define_Tag;
	
	
?>