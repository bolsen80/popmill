<?LassoScript

// ******************************************************************************************
// [POPGet -> (Message)]
//
// This member tag allows access the different parts of downloaded messages.
// Returns nothing if the field is not found.
// ******************************************************************************************
	
Define_Tag: 'Message';

	if: ((self->'PM_connection')->'PM_errorMessage') == 'No error';
		library: $__POPmill_pgMi__;
		
		
		local: 'PM_pgID'   			  = (self -> 'PM_pID');
		local: 'PM_pField' 			  = (named_param: '-Field'); 
		local: 'PM_pgMessageCount' 	  = (self -> 'PM_messageCount');
		local: 'PM_pgAryMessage' 	  = (self -> 'PM_aryMessage');
		local: 'PM_pgAllHeaderFields' = (self->'PM_pAllHeaderFields');
		local: 'PM_mapHeaderField'	  = map; // dual purpose
		local: 'PM_tCurMsg'			  = map;
		
		if: #PM_pgID != '';
			#PM_pID = 1;
		/if;
				
		if: #PM_pgID == '';
			Fail_If: #PM_pID == '', 3, 'The -id or -uid parameters were not used or defined in [POPGet -> (Message)]';
			Fail_If: #PM_pField == '', 3, 'The -Field parameter was not used or defined in [POPGet -> (Message)]';
		else: #PM_pgID != '';
			Fail_If: #PM_pField == '', 3, 'The -Field parameter was not used or defined in [POPGet -> (Message)]';
		/if;
		
		
		if: #PM_pID != 'false';
		
			if: #PM_pgMessageCount != 0 && #PM_pgAryMessage->size != 0;
			
				if: #PM_pID <= #PM_pgMessageCount;
					
					if: #PM_pgAllHeaderFields || #PM_pField == 'Body' 
					|| #PM_pField == 'Raw' || #PM_pField == 'Header';
						
						if: #PM_pField->type == 'string';	
						
							return: #PM_pgAryMessage->get: #PM_pID->find: #PM_pField;
							
						else: #PM_pField->type == 'array';	
							
							loop: #PM_pgAryMessage->get:#PM_pID->size;
								
								#PM_tCurMsg = #PM_pgAryMessage->get: #PM_pID;
								
								loop: #PM_pField->size;
									if: #PM_tCurMsg->find:#PM_pField->get:loop_count != '';
										#PM_mapHeaderField->insert:(#PM_pField->get:loop_count) = (#PM_tCurMsg->find:#PM_pField->get:loop_count);
									/if;
								/loop;
							
						/loop;
						
							return: #PM_mapHeaderField;
						/if;
					
					
					else; // no header fields available

						#PM_mapHeaderField = (parse_header:  #PM_pgAryMessage->get: #PM_pID->find: 'Header', -decodeheader, -Field=#PM_pField);
						
						
						if: #PM_pField->type == 'array';
							loop: #PM_pField->size;
							
								if: #PM_pField->get:loop_count == 'body' ||
									#PM_pField->get:loop_count == 'header' ||
									#PM_pField->get:loop_count == 'raw';
									
									#PM_mapHeaderField->insert: (#PM_pField->get:loop_count) = (#PM_pgAryMessage->get:#PM_pID->find: (#PM_pField->get:loop_count));
								/if;
							/loop;
						/if;
						
						
						if: #PM_pField->type == 'string' && #PM_mapHeaderField->size != 0;
							return: #PM_mapHeaderField->get:1->second;
						else: #PM_pField->type == 'array' && #PM_mapHeaderField->size != 0;
							return: #PM_mapHeaderField;
						/if;
					/if;
				/if;
			/if;
		/if;

	else;
		
		return: '';
	
	/if;
			
				
	/Define_Tag;


?>