<?LassoScript

// ************************************************************************************************
// [POPGet -> (Addresses)]
//
// Gets and separates the addresses from To, From and CC.
// ************************************************************************************************

	Define_Tag: 'Addresses';
		
		if: (self->'PM_connection')->'PM_errorMessage' == 'No error';

			library: $__POPmill_pgMi__;
			
			local: 'PM_pField' 			    = (named_param: '-Field');
			local: 'PM_pgAllHeaderFields'   = (self->'PM_pAllHeaderFields');
			local: 'PM_pgAryMessage'	    = (self->'PM_aryMessage');
			local: 'PM_pgMessageCount'	    = (self->'PM_messageCount');
			local: 'PM_pgID'			    = (self->'PM_pID');
			local: 'PM_pgMessageDownloaded' = (self->'PM_messageDownloaded');
			
			if: #PM_pgID != '';
				#PM_pID = 1;
			/if;

		
			Fail_If: #PM_pField == '', 2, 'The -Field parameter was not used or defined in [POPGet -> (Addresses)]';				
				
				if: #PM_pField->type == 'string';
					if: #PM_pgID == '' && #PM_pID != 'false';
						Fail_If: #PM_pID == '', 3, 'The -id or -uid parameters were not used or defined in [POPGet -> (Addresses)]';
						Fail_If: #PM_pField != 'To' && #PM_pField != 'From' && #PM_pField != 'CC' && #PM_pField != 'Reply-To', 
						 2, 'The -Field parameter contains an invalid value in [POPGet -> (Addresses)]';
					else: #PM_pgID != '';
						Fail_If: #PM_pField != 'To' && #PM_pField != 'From' && #PM_pField != 'CC' && #PM_pField != 'Reply-To', 
						 2, 'The -Field parameter contains an invalid value in [POPGet -> (Addresses)]';
						#PM_pID = 1;
					/if;
				else: #PM_pField->type == 'array';
				
					loop: #PM_pField->size;
				
						if: #PM_pgID == '' && #PM_pID != 'false';
							Fail_If: #PM_pID == '', 3, 'The -id or -uid parameters were not used or defined in [POPGet -> (Addresses)]';
							Fail_If: #PM_pField->(get:loop_count) != 'To' && #PM_pField->(get:loop_count) != 'From' && #PM_pField->(get:loop_count) != 'CC' && #PM_pField->(get:loop_count) != 'Reply-To', 
							 2, 'The -Field parameter contains an invalid value in [POPGet -> (Addresses)]';
						else: #PM_pgID != '';
							Fail_If: #PM_pField->(get:loop_count) != 'To' && #PM_pField->(get:loop_count) != 'From' && #PM_pField->(get:loop_count) != 'CC' && #PM_pField->(get:loop_count) != 'Reply-To', 
							 2, 'The -Field parameter contains an invalid value in [POPGet -> (Addresses)]';
							#PM_pID = 1;
						/if;
						
					/loop;
				/if;
				
					if: #PM_pgAllHeaderFields;
						if: #PM_pgMessageCount >= #PM_pID || #PM_pgMessageDownloaded >= #PM_pID;
							return: (POP_addresses: -header=(#PM_pgAryMessage->get:#PM_pID), -field=#PM_pField);	
						/if;
					else;
						if: #PM_pgMessageCount >= #PM_pID || #PM_pgMessageDownloaded >= #PM_pID;
							return: (POP_addresses: -header=(parse_header: (#PM_pgAryMessage->get:#PM_pID->find:'raw'), -decodeheader), -field=#PM_pField);	
						/if;
					/if;
			
			/if;
			
		/Define_Tag;	

?>