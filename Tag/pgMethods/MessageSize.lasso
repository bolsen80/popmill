<?LassoScript

// ************************************************************************************
// [POPGet -> (MessageSize)]
//
// Outputs size of messages
// ************************************************************************************

	Define_Tag: 'MessageSize';
	
		library: $__POPmill_pgMi__;

		local: 'PM_PGID'            	= (self -> 'PM_pID');
		local: 'PM_aryMessageSizes' 	= (self->'PM_aryMessageSizes');
		local: 'PM_messageCount'    	= (self -> 'PM_messageCount');
		local: 'PM_pNewMessages'		= false;
		local: 'PM_pgNewMessages'		= (self->'PM_pNewMessages');
		local: 'PM_aryLIST'				= (self->'PM_aryLIST');
		local: 'PM_aryNewMsgFinal'		= array;
		local: 'PM_aryDownloadSpecific' = (self->'PM_aryDownloadSpecific');
		local: 'PM_tCurrentMsg'			= '';
		local: 'PM_pgID'				= (self->'PM_pID');

			Loop: params->size;
				 if: params->get:loop_count == '-newMessages';
					#PM_pNewMessages = true;
					loop_abort;
				 /if;
		    /Loop;
			    

		  	if: #PM_PGID != '';
		  		
		  			
		  		loop: #PM_aryLIST->size;
		  			if: #PM_aryLIST->get:loop_count->split:' '->get:1 == #PM_PGID;
		  					return: #PM_aryLIST->get:loop_count->split:' '->get:2;
		  			/if;
		  		/loop;
		  		
		  		
		  			
		  	else: #PM_pNewMessages && #PM_pgNewMessages && #PM_pgID == '';
		  	

		  		loop: #PM_aryDownloadSpecific->size;
		  			#PM_tCurrentMsg = #PM_aryDownloadSpecific->get:loop_count;		
		  						
		  			loop: #PM_aryLIST->size;
		  						  				
		  				if: #PM_aryLIST->get:loop_count->split:' '->get:1 == #PM_tCurrentMsg;
							#PM_aryNewMsgFinal->insert: (integer: #PM_aryLIST->get:loop_count->split:' '->get:2);
							loop_abort;
						/if;
		  			
		  			/loop;
		  		
		  		/loop;
		  		
		  		return: #PM_aryNewMsgFinal;
		  		
		  		
		  		
		  		
		  	else: #PM_pID != '' && #PM_pID != 'false';
	    		  
		  			if: #PM_aryLIST->size != 0 && (integer: #PM_pID) <= (integer: #PM_messageCount);
		  				return: (integer: #PM_aryLIST->get:#PM_pID->split:' '->get:2);
		  			/if;
		  		
		  		
		  	else: #PM_pID == '';
	    		    return: #PM_aryMessageSizes; 
		 	 /if;		
			 
	/Define_Tag;


?>