<?LassoScript

// ************************************************************************************
// [POPGet -> (UniqueID)]
//
// Gets the Unique ID string set by the POP3 server.
// ************************************************************************************
	
	Define_Tag: 'UniqueID';
		
		
		if: ((self->'PM_connection')->'PM_errorMessage') == 'No error';
		
			local: 'PM_pID'            = (named_param: '-ID');
			local: 'PM_pgID'           = (self -> 'PM_pID');
			local: 'PM_aryUniqueID'    = (self -> 'PM_aryUniqueID');
			local: 'PM_messageCount'   = (self -> 'PM_messageCount');
			local: 'PM_pgAryNewUID'    = (self -> 'PM_aryNewUID');
			local: 'PM_pgNewMessages'  = (self -> 'PM_pNewMessages');
			local: 'PM_pNewMessages'   = false;
			local: 'PM_rFinal'		   = array;

			
			Loop: params->size;
				 if: params->get:loop_count == '-newMessages';
					local: 'PM_pNewMessages' = true;
					loop_abort;
				 /if;
		    /Loop;
			    
		
			if: #PM_pNewMessages && #PM_pgNewMessages && #PM_pgID == '';

					return: #PM_pgAryNewUID;
		
			else: #PM_pgID != '';
				if: #PM_aryUniqueID->size != 0;
			  		
			  		loop: #PM_aryUniqueID->size;
			  			if: #PM_aryUniqueID->get:loop_count->split:' '->get:1 == #PM_pgID;
							return: #PM_aryUniqueID->get:loop_count->split:' '->get:2;
						/if;
					/loop;
					
			  	/if;
		  	
		  	
		  	
			 else: #PM_pID != '';
		    	 
		    	 if: #PM_aryUniqueID->size != 0 && #PM_pID <= #PM_messageCount;
			  		 return: #PM_aryUniqueID->get:#PM_pID->(split:' ')->get:2;
			  	 /if;
		  	  
		 	 else: #PM_pID == '';
		  	
		  		loop: #PM_aryUniqueID->size;
		  			#PM_rFinal->insert: (#PM_aryUniqueID->get:loop_count->split:' '->get:2);
		  		/loop;
		  	
		  		return: #PM_rFinal;
		  		
		  	/if;
		  	  	
		/if;
			
	/Define_Tag;

?>