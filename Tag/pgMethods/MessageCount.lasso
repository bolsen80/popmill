<?LassoScript

// ************************************************************************************
// [POPGet->(MessageCount)]
//
// Gets the number of messages in a user's mailbox.
// ************************************************************************************

	
	Define_Tag: 'MessageCount';
	
		return: (integer: (self -> 'PM_messageCount'));
		
	/Define_Tag;
	
// ************************************************************************************

?>