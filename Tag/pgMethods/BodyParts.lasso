<?LassoScript

// ************************************************************************************
// [POPGet -> (BodyParts)]
//
// Outputs the MIME body array.
// ************************************************************************************

	
	Define_Tag: 'BodyParts';
		
		library: $__POPmill_pgMi__;
		local: 'PM_PGID' 		  	 = (self->'PM_pID');
		local: 'PM_pAllBodyParts' 	 = (self->'PM_pAllBodyParts');
		local: 'PM_aryMessage'	  	 = (self->'PM_aryMessage');
		local: 'PM_messageBodyParts' = (self->'PM_messageBodyParts');
		
		
		if: ((self->'PM_connection')->'PM_errorMessage') == 'No error';
				
			if: #PM_PGID != '';
				
				if: #PM_pAllBodyParts == false;
					if: #PM_aryMessage->size==1;
						return: (parse_body: -message=(#PM_aryMessage->get: 1->find: 'raw'));
					else;
						return array;
					/if;
				else;
				
					if: #PM_messageBodyParts->size == 1;		
						return: #PM_messageBodyParts->get:1;
					else;
						return: array;
					/if;
				/if;
			/if;

			if: #PM_pID != 'false' && #PM_pID != '';	
				
				if: #PM_pAllBodyParts == false;					
					if: #PM_aryMessage->size >= #PM_pID;
						return: (parse_body: -message=(#PM_aryMessage->get: #PM_pID->find: 'raw'));
					else;
						return: array;
					/if;
				else;
				
					if: #PM_messageBodyParts->size >= (integer: #PM_pID);
						return: #PM_messageBodyParts->get: #PM_pID;
					else;
						return: array;
					/if;
				/if;
				
			else;
				return: #PM_messageBodyParts;
			/if;
		
		else;	
		
			return: array;
		
		/if;
	
	/Define_Tag;

?>