<?LassoScript

// ----------------------------------------------------------------------------------------------------------
// [POPGet -> (AllMessages)]
//
// Outputs all the messages in an array.
// ----------------------------------------------------------------------------------------------------------
	
	Define_Tag: 'AllMessages';
	
		if: ((self->'PM_connection')->'PM_errorMessage') == 'No error';

			return: (self -> 'PM_aryMessage');
			
		else;
			
			return: array;
			
		/if;
	
	/Define_Tag;
	
?>