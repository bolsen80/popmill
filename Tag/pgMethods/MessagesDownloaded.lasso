<?LassoScript

// ************************************************************************************	
// [POPGet -> (MessagesDownloaded)]
//
// The amount of messages in queue might be different than the actual number of messages downloaded.
// ************************************************************************************	

	Define_Tag: 'MessagesDownloaded';
	
		return: (integer: (self -> 'PM_messageDownloaded'));
	
	/Define_Tag;
	
?>