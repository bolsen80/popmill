<?LassoScript

	/* Communication Tags
		
		[pop_stat]
		[pop_list]
		[pop_top]		
		[pop_messagecount]
		[pop_allmessagesizes]
		[pop_messagesize]
		[pop_uniqueid]
		[pop_newmessages]
		[pop_header]
		[pop_body]
		[pop_bodyparts]
		[pop_message]
		[pop_addresses]
		[pop_delete]
		[pop_undelete]
		[pop_command]
	*/
		
	
// ********************************************************************************************

	// [POP_STAT]
	// 11/14/2002 Converted For __POPmill__

	Define_Tag: 'POP_STAT';
	

		library: $__POPmill_tcl__;
		
		local: 'PM_STAT' = @#PM_connection->'PM_stat';
		__POPmill_close__: #PM_pName, #PM_connectWithin;
		
		return: #PM_STAT;
			
	/Define_Tag;
	
// ********************************************************************************************
	
	// [POP_LIST]		
	// 11/17/2002 Converted For __POPmill__
	
	
	Define_Tag: 'POP_LIST';
		
		library: $__POPmill_tcl__;
		
		local: 'PM_rFinal' = '';
		local: 'PM_LIST' = '';
		local: 'PM_messageCount' = '';
		
		
			library: $__POPmill_udl__;	

		 	#PM_messageCount = @#PM_connection->'PM_messageCount';	// get message count

		 	if: #PM_pID != '' && #PM_pID != 'false' && #PM_messageCount != 0;
				#PM_rFinal = @((#PM_connection->'PM_list')->get: #PM_pID);			
			 else: #PM_pID == '' && #PM_messageCount != 0;
				#PM_rFinal = @#PM_connection->'PM_list';				
			 /if;
			
			 __POPmill_close__: #PM_pName, #PM_connectWithin;
			 
		
		if: #PM_pID == '' && #PM_messageCount == 0;
			return: array;
		else;
			return: #PM_rFinal;
		/if;
	
	/Define_Tag;
	
	
// ********************************************************************************************


	// [POP_TOP]
	// 11/17/2002 Converted For __POPmill__
	

	Define_tag: 'POP_TOP';
	
		library: $__POPmill_tcl__;
		
		local: 'PM_top' = '',
		  	   'PM_messageCount' = '',
		  	   'PM_pLimit' = (named_param: '-limit');
		  	   
		if: #PM_connection->'PM_errorMessage' == 'No error';
			library: $__POPmill_udl__;

			Fail_If: #PM_pID == '', 2,  'The -id or -uid parameters were not used or defined in [POP_TOP]';
	
					
			#PM_messageCount = @#PM_connection->'PM_messageCount';	// get message count
		
			if: (integer: #PM_pID) <= (integer: #PM_messageCount) && #PM_pID != 'false';
				
				if: #PM_pLimit != '';
					
					#PM_top = #PM_connection->sendCommand: ('TOP ' + #PM_pID + ' ' + #PM_pLimit), true, -encodeNone;
					#PM_top = (String_RemoveTrailing: -Pattern='\r\n.\r\n', #PM_top);

				else;
			
					#PM_top = #PM_connection->sendCommand: ('TOP ' + #PM_pID + ' 0'), true, -encodeNone;
					#PM_top = (String_RemoveTrailing: -Pattern='\r\n.\r\n', #PM_top);

				/if;
				
			/if;
			
			__POPmill_close__: #PM_pName, #PM_connectWithin;
			
		/if;
		
		
		return: #PM_top;
		
	/Define_Tag;


// ********************************************************************************************
	
	// [POP_MessageCount]
	// 11/14/2002 Converted For __POPmill__
	
	Define_Tag: 'POP_MessageCount';
	
		library: $__POPmill_tcl__;
		
		local: 'PM_messageCount' = @#PM_connection->'PM_messageCount';
		__POPmill_close__: #PM_pName, #PM_connectWithin;
		
		return: (integer: #PM_messageCount);

	/Define_Tag;
	
	
// ********************************************************************************************

	// [POP_AllMessageSizes]
	// 11/14/2002 Converted For __POPmill__

	
	Define_Tag: 'POP_AllMessageSizes';
		
		library: $__POPmill_tcl__;
		
		local: 'PM_allMessageSize' = @#PM_connection->'PM_allMessageSize';
		__POPmill_close__: #PM_pName, #PM_connectWithin;
		
		return: (integer: #PM_allMessageSize);
	
	/Define_Tag;



// ********************************************************************************************


	// [POP_MessageSize]
	// 11/17/2002 Converted For __POPmill__
	
	
	Define_Tag: 'POP_MessageSize';

	// Connection and ID check libraries
	library: $__POPmill_tcl__;
	
	// Variable set
	local: 'PM_rFinal' = string,
		   'PM_messageCount' = 0,
		   'PM_stat' = string,
		   'PM_listData' = '';
		   
	
	if: #PM_connection->'PM_errorMessage' == 'No error';
	
		library: $__POPmill_udl__;

		#PM_messageCount = @#PM_connection->'PM_messageCount';	// get message count
		
		if: #PM_pID != '' && #PM_pID != 'false' && #PM_messageCount != 0;
		
			#PM_rFinal = @(#PM_connection->'PM_messageSizes')->get:#PM_pID;
		
		else: #PM_pID == '' && #PM_messageCount != 0;
		
			#PM_rFinal = array; // cast #PM_rFinal to type array
			#PM_rFinal = @#PM_connection->'PM_messageSizes';

		/if;
		
		__POPmill_close__: #PM_pName, #PM_connectWithin;
		
	/if;
	
	
	
	if: #PM_messageCount == 0 && (#PM_pID == '' && ((named_param: '-id') == '' && (named_param: '-uid') == '')); // No message; no id specified
		return: array;
	else: #PM_messageCount != 0 && (#PM_pID == '' && ((named_param: '-id') == '' || (named_param: '-uid') == '')); // messages; no id specified
		return: #PM_rFinal;
	else: #PM_messageCount == 0 && #PM_pID != '' && #PM_pID != 'false'; // no message; id specified
		return: (integer);
	else: #PM_messageCount != 0 && #PM_pID != '' && #PM_pID != 'false'; // messages; id specified
		return: (integer: #PM_rFinal);
	/if;
	
/Define_Tag;



// ********************************************************************************************


// [POP_UniqueID]
// 11/17/2002 Converted For __POPmill__

	
define_tag: 'POP_UniqueID';

	library: $__POPmill_tcl__;
	
	local: 'PM_pID'		 = (named_param: '-id');
	local: 'PM_pUID'	 = (named_param: '-uid');
	local: 'PM_tUidBuf'  = array;
	
	if: #PM_pID == '';
		local: 'PM_rFinal'   = array;
	else;
		local: 'PM_rFinal' 	 = string;
	/if;

	#PM_tUidBuf = @#PM_connection->'PM_uidl';
			
	// get unique ids
		
	if: #PM_pID == '' && #PM_pUID == '';
					
		loop: #PM_tUidBuf->size;
			#PM_rFinal->insert:(#PM_tUidBuf->get:loop_count->split:' '->get:2);
		/loop;
				
	else: #PM_pUID != '';
		
		   loop: #PM_tUidBuf->size;
		   		if: #PM_tUidBuf->get:loop_count->split:' '->get:2 == #PM_pUID;
		   			#PM_rFinal = #PM_tUidBuf->get:loop_count->split:' '->get:1;
		   		/if;
		   /loop;
		   	
	else: #PM_pID != '';
			
		if: #PM_connection->'PM_errorMessage' == 'No error';
			#PM_rFinal = #PM_tUidBuf->get:#PM_pID->split:' '->get:2;	
		/if;
					
	/if;
		
		
	__POPmill_close__: #PM_pName, #PM_connectWithin;
		
	
	return: #PM_rFinal;
	
/define_tag;



// ********************************************************************************************


// [POP_NewMessages]
// 11/17/2002 Converted For __POPmill__


	Define_Tag: 'POP_NewMessages';				
			
			local: 'PM_pUniqueID'   		= false;
			local: 'PM_pUpdate'	  			= false;
			local: 'PM_pReturnBoth' 		= false;
			local: 'PM_pUIDList'			= (named_param: '-uidlist');
			local: 'PM_pDatabase'			= (named_param: '-database');
			local: 'PM_pDBUsername'			= (named_param: '-dbusername');
			local: 'PM_pDBPassword'			= (named_param: '-dbpassword');
			local: 'PM_aryUniqueID' 		= array;
			local: 'PM_uniqueID'			= string;
			local: 'PM_hostID'	   		 	= integer;
			local: 'PM_aryOldMsg'			= array;
			local: 'PM_aryFinID'			= array;
			local: 'PM_aryFinUID'			= array;			
			local: 'PM_arySvUID'			= array;
			local: 'PM_qUID'				= string;
			local: 'PM_qDelUID'				= string;
			local: 'PM_qAddUID'				= string;
			local: 'PM_tUniqSize'			= 0;
			local: 'PM_tOldMsg'				= false;
			local: 'PM_tAryCurrUID'			= array;
			local: 'PM_tCurrID'				= string;
			local: 'PM_tCurrUID'			= string;
			local: 'PM_aryRBRes'			= array;
			local: 'PM_pHostName'			= '';
			local: 'PM_pUsername'			= '';
						
			 Loop: params->size;
				 if: params->get:loop_count == '-uniqueid' || params->get:loop_count == '-uniqueids';
					#PM_pUniqueID = true;
				 else: params->get:loop_count == '-update';
				 	#PM_pUpdate = true;
				 else: params->get:loop_count== '-returnboth';
				 	#PM_pReturnBoth = true;
				 /if;
			/loop;
			 
			if: #PM_pDatabase == '';
				#PM_pDatabase = 'popmill';
			/if;
			
			    
			 
			fail_if: (named_param: '-dbusername') == '', '2', 'The -dbusername parameter was not used or defined.';    			
			fail_if: (named_param: '-dbpassword') == '', '2', 'The -dbpassword parameter was not used or defined.';
			  
			
				if: #PM_pUIDList == '';
						
						library: $__POPmill_tcl__;			
						
						if: #PM_connection->'PM_errorMessage' == 'No error';
						
							#PM_aryUniqueID = @#PM_connection->'PM_uidl';
							#PM_pHostName   = @#PM_connection->'PM_hostName';
							#PM_pUserName   = @#PM_connection->'PM_userName';	
										
							__POPmill_close__: #PM_pName, #PM_connectWithin;
						/if;
				else;
					library: $__POPmill_rtl__;
					#PM_aryUniqueID = #PM_pUIDList;
					#PM_pHostName   = (named_param: '-host');
					#PM_pUserName	= (named_param: '-user');
				/if;
					
					
						
				inline: -database = #PM_pDatabase, -username=#PM_pDBUsername, -password=#PM_pDBPassword,
						-SQL = 'SELECT * FROM popmill.host WHERE server = "' + #PM_pHostName + '" AND username = "' + #PM_pUserName + '"';	
						if: (found_count) == 0;
							
							inline: -database = #PM_pDatabase, -username=#PM_pDBUsername, -password=#PM_pDBPassword,
									-SQL = 'INSERT INTO popmill.host SET server = "' + #PM_pHostName + '", username = "' + #PM_pUserName + '"';	
							/inline;
											
							inline: -database = #PM_pDatabase, -username=#PM_pDBUsername, -password=#PM_pDBPassword,
									-SQL = 'SELECT * FROM popmill.host WHERE server = "' + #PM_pHostName + '" AND username = "' + #PM_pUserName + '"';	
												
									#PM_hostID = (field: 'id');
											
								/inline;
									
						else;
								
								#PM_hostID = (field: 'id');
								
						/if;
										
				/inline;

							 						    	
					#PM_qUID = ('SELECT id, uid FROM uid WHERE host_id=' + #PM_hostID + ' AND ');	
					loop: #PM_aryUniqueID->size;
						#PM_qUID = (#PM_qUID + 'uid="' + (__POPMill_CleanSQLQueries__: #PM_aryUniqueID->get:loop_count->split:' '->get:2) + '"');
					    		
						if: loop_count != #PM_aryUniqueID->size;
						   #PM_qUID = (#PM_qUID + ' OR ');
						/if;
						    		
						    #PM_tUniqSize = (loop_count);
					/loop;
					    
					    
					inline: -database = #PM_pDatabase, -username=#PM_pDBUsername, -password=#PM_pDBPassword,
					        -SQL = (#PM_qUID);
					    	records;
					    		#PM_aryOldMsg->insert: (field: 'uid');
					    	/records;
					/inline;

					    						    	
					loop: #PM_aryUniqueID->size;
					   #PM_tOldMsg = false;
					   
					   
					    #PM_tCurrID  = #PM_aryUniqueID->get:loop_count->split: ' '->get:1;
					    #PM_tCurrUID = #PM_aryUniqueID->get:loop_count->split: ' '->get:2;
					    	
					    loop: #PM_aryOldMsg->size;
					    	if: #PM_tCurrUID == #PM_aryOldMsg->get:loop_count;
					    		#PM_tOldMsg = true;
								#PM_arySvUID->insert:(#PM_aryOldMsg->get:loop_count);
					    	/if;
					    /loop;

					    if: #PM_tOldMsg == false;
					    	#PM_aryFinID->insert: #PM_tCurrID;
					    	#PM_aryFinUID->insert: #PM_tCurrUID;
					    /if;

					 /loop;
					 
					
					 
				if: #PM_pUpdate;	
					    		
					 if: #PM_arySvUID ->size != 0;
					    		
					    	#PM_qDelUID = ('DELETE FROM uid WHERE host_id=' + #PM_hostID + ' AND ');
					    	
					    	loop: #PM_arySvUID->size;
					    		#PM_qDelUID = (#PM_qDelUID + ' uid !=' + '"' + (__POPMill_CleanSQLQueries__: #PM_arySvUID->get:loop_count) + '"');
					    			
					    		if: loop_count != #PM_arySvUID->size;
					    			#PM_qDelUID = (#PM_qDelUID + ' AND ');
					    		/if;
					    	/loop;
					    		
					    	inline: -database = #PM_pDatabase, -username=#PM_pDBUsername, -password=#PM_pDBPassword,
					    			-SQL = #PM_qDelUID;
					    	/inline;

					  else;
					    	
					    	inline: -database = #PM_pDatabase, -username=#PM_pDBUsername, -password=#PM_pDBPassword,
					    			-SQL=('DELETE FROM uid WHERE host_id=' + #PM_hostID);
					    	/inline;
					    	
					   /if;	
					    	
					   
					    	
					    	
					    if: #PM_aryFinUID->size != 0;
					    	
					    	#PM_qAddUID = 'INSERT INTO uid (host_id, uid) VALUES ';

					    	loop: #PM_aryFinUID->size;
					    		#PM_qAddUID = (#PM_qAddUID + '(' + #PM_hostID + ',' + '"' + (__POPMill_CleanSQLQueries__: #PM_aryFinUID->get:loop_count) + '")');
					    			
					    		if: loop_count != #PM_aryFinUID->size;
					    			#PM_qAddUID = (#PM_qAddUID + ',');
					    		/if;
					    			
					   		/loop;
					   
					   		
					   		inline: -database=#PM_pDatabase, -username=#PM_pDBUsername, -password=#PM_pDBPassword,
					   				-SQL = #PM_qAddUID;
					   		
					   		/inline;	
					   		
					   	/if;
					   	
					 /if;
					    
					    
					    if: #PM_pReturnBoth;
					    	#PM_aryRBRes->insert: #PM_aryFinUID;
					    	#PM_aryRBRes->insert: #PM_aryFinID;
							return: #PM_aryRBRes;
					    	 
					    else: #PM_pUniqueID;
					    
					    	if: #PM_aryFinUID->size != 0;
					    		return: #PM_aryFinUID;
					    	else;
					    		return: array;
					    	/if;
					    	
					    else;
					    
					   		if: #PM_aryFinID->size != 0;
					    		return: #PM_aryFinID;
					    	else;
					    		return: array;
					    	/if;
							
					    /if;
					   					    					    
	/Define_Tag;

	
	
// ********************************************************************************************


	// [POP_Header]
	// 11/17/2002 Converted For __POPmill__
	
	Define_Tag: 'POP_Header';

		library: $__POPmill_tcl__;
		
		local: 'PM_header' = '';
		local: 'PM_pParse' = false;
		local: 'PM_pDontDecode' = false;
		local: 'PM_messageCount' = '';
		
		 Loop: params->size;
			if: params->get: loop_count == '-parse';
				#PM_pParse = true;
			else: params->get: loop_count == '-dontdecode';
				#PM_pDontDecode = true;
			 /if;
			/Loop;
		
		if: #PM_pParse;
			#PM_header = map;
		/if;
		
		if: #PM_connection->'PM_errorMessage' == 'No error';

				library: $__POPmill_udl__;
			
				Fail_If: #PM_pID == '', 2,  'The -id or -uid parameters were not used or defined in [POP_Header]';
	
				#PM_messageCount = @#PM_connection->'PM_messageCount';	// get message count
			
			
				if: #PM_pID <= integer: #PM_messageCount && #PM_pID != 'false';	
						#PM_header = #PM_connection->sendCommand: ('TOP ' + #PM_pID + ' 0'), true, -encodeNone;
						#PM_header = (string_removetrailing: -Pattern='\r\n.\r\n', #PM_header);
						#PM_header = (string_removeLeading: -Pattern=#PM_header->split:'\r\n'->get: 1, #PM_header);
						#PM_header = (string_removeleading: -Pattern='\r\n', #PM_header);
						#PM_header = (string_removetrailing: -Pattern='\r\n', #PM_header);
						
						if: #PM_pDontDecode == false;
							#PM_header = (decode_mimeheader: #PM_header);
						/if;
															
						if: #PM_pParse;
							#PM_header = (parse_header: #PM_header, -decodeheader);
						/if;
				/if;
			/if;
			
			__POPmill_close__: #PM_pName, #PM_connectWithin;		
				
		return: #PM_header;
		
	/Define_Tag;
	
	
// ********************************************************************************************


	// [POP_Body] 
	// 11/17/2002 Converted For __POPmill__

	
	Define_Tag: 'POP_Body';

			library: $__POPmill_tcl__;
			
			local: 'PM_body' = '';
			local: 'PM_message' = '';
			local: 'PM_pParse' = false;
			local: 'PM_header' = '';
			local: 'PM_parsedBody' = '';
			local: 'PM_messageCount' = '';
			local: 'PM_pDontDecode' = false;
			local: 'PM_cte' = '';
			
			
		if: #PM_connection->'PM_errorMessage' == 'No error';
			
			library: $__POPmill_udl__;
			
			Fail_If: #PM_pID == '', 2,  'The -id or -uid parameters were not used or defined in [POP_Body]';
			
			Loop: params->size;
				 if: params->get:loop_count == '-parse';
					#PM_pParse = true;
				 else: params->get:loop_count == '-dontdecode';
				 	#PM_pDontDecode = true;			
				 /if;
			/Loop;
			
			if: #PM_pParse;
				#PM_body =  array;
			/if;		    
			    
			#PM_messageCount = @#PM_connection->'PM_messageCount';

			if: #PM_pID != 'false' && (integer: #PM_pID) <= (integer: #PM_messageCount);	
					
					#PM_message = #PM_connection->sendCommand: ('RETR ' + #PM_pID), true, -encodeNone;
					#PM_header = #PM_message->split: '\r\n\r\n'->get: 1;
					#PM_body = (String_removeLeading: #PM_message, -Pattern=#PM_header);
					#PM_body = (string_removeTrailing: -Pattern='\r\n.\r\n', #PM_body);
					
					
					if: #PM_pDontDecode == false;
					
						local: 'PM_cte' = (string_findregexp: -find = 'Content-Transfer-Encoding:\\s*.+', #PM_header, -ignorecase);
						
						if: #PM_cte->size != 0;
																
							if: #PM_cte->get:1 >> 'quoted-printable';
								#PM_body = (decode_qp_body: #PM_body);
							else: #PM_cte->get:1 >> 'base64';
								#PM_body = (string_replace: -find='\r\n', -replace='', #PM_body);
								#PM_body = (string_replace: -find='\r', -replace='', #PM_body);
								#PM_body = (string_replaceregexp: -find='\\s', -replace='', #PM_body);
								#PM_body = (string_replace: -find='\t', -replace='', #PM_body);
								#PM_body = (decode_base64: #PM_body);
							/if;
							
						  /if;
					/if; 		
					
							
					#PM_body= $__POPmill_Info__->ab: #PM_body;

					if: #PM_pParse;
						#PM_parsedBody = (Parse_Body: -message=#PM_message);
						
						if: #PM_parsedBody -> size == 0;
							#PM_parsedBody = #PM_body;
						/if;
					/if;
			/if;
			
			__POPmill_close__: #PM_pName, #PM_connectWithin;
		
		/if;
		
				
		if: #PM_parsedBody != '';
			return: #PM_parsedBody;
		else;
			return: #PM_body;
		/if;
		
	/Define_Tag;


// ********************************************************************************************


	// [POP_BodyParts]
	// 11/17/2002 Converted For __POPmill__

	
	// This tag essentially does the same thing as [POP_Body: -name=$name, -id=$id, -parse]
	
		Define_Tag: 'POP_BodyParts';
			
			library: $__POPmill_tcl__;
			
			if: #PM_connection->'PM_errorMessage' == 'No error';
			
				library: $__POPmill_udl__;
				
				Fail_If: #PM_pID == '', 2,  'The -id or -uid parameters were not used or defined in [POP_BodyParts]';
				
				local: 'PM_parsedBody' = array;
				local: 'PM_messageCount' = '';
				local: 'PM_message' = '';
			
		
				#PM_messageCount = @#PM_connection->'PM_messageCount';

				if: (integer: #PM_pID) <= (integer: #PM_messageCount) && #PM_pID != 'false';	
					
					#PM_message = #PM_connection->sendCommand: ('RETR ' + #PM_pID), true, -encodeNone;	
					#PM_parsedBody = (Parse_Body: -message = #PM_message);
					
				/if;
							
					__POPmill_close__: #PM_pName, #PM_connectWithin;

			/if;
				
													
				
		return: #PM_parsedBody;
		
		
	/Define_Tag;


// ********************************************************************************************


		// [POP_Message]
		// 11/17/2002 Converted For __POPmill__

	
		Define_Tag: 'POP_Message';

		library: $__POPmill_tcl__;
	
		if: #PM_connection->'PM_errorMessage' == 'No error';

			library: $__POPmill_udl__;

			Fail_If: #PM_pID == '', 2,  'The -id or -uid parameters were not used or defined in [POP_Message]';
			
			local: 'PM_pParse' = false;
			local: 'PM_messageCount' = '';
			local: 'PM_message' = '';
			local: 'PM_parsedMessage' = '';
			local: 'PM_pDontDecode' = false;
			local: 'PM_header' = '';
			local: 'PM_body' = '';
			local: 'PM_cte'  = '';
			
			 Loop: params->size;
				if: params->get:loop_count == '-parse';
					#PM_pParse = true;
				else: params->get:loop_count == '-dontdecode';
					#PM_pDontDecode = true;
				 /if;
			 /Loop;
			 
			 if: #PM_pParse;
			 	#PM_parsedMessage = map;
			 /if;
			
			#PM_messageCount = @#PM_connection->'PM_messageCount';	// get message count

			if: (integer: #PM_pID) <= (integer: #PM_messageCount) && #PM_pID != 'false';
				
					#PM_message = #PM_connection->sendCommand: ('RETR ' + #PM_pID), true, -encodeNone;
					#PM_message = (string_removeTrailing: -Pattern='\r\n.\r\n', #PM_message);
					#PM_header = #PM_message->split: '\r\n\r\n'->get: 1;
					#PM_body = (String_removeLeading: -Pattern=#PM_header, #PM_message);
					
					if: #PM_pDontDecode == false;
						
						local: 'header' = (decode_mimeheader: #PM_header);
						#PM_cte = (string_findregexp: -find = 'Content-Transfer-Encoding:\\s*.+', #PM_header, -ignorecase);
						
						
						if: #PM_cte->size != 0;
							if: #PM_cte->get:1 >> 'quoted-printable';
								#PM_body = (decode_qp_body: #PM_body);
							else: #PM_cte->get:1 >> 'base64';
								#PM_body = (string_replace: -find='\r\n', -replace='', #PM_body);
								#PM_body = (string_replace: -find='\r', -replace='', #PM_body);
								#PM_body = (string_replaceregexp: -find='\\s', -replace='', #PM_body);
								#PM_body = (string_replace: -find='\t', -replace='', #PM_body);
								#PM_body = (decode_base64: #PM_body);
							/if;
						/if;
					
					
					
					/if;
					
					#PM_body= $__POPmill_Info__->ab: #PM_body;

					
					if: #PM_pParse;
						#PM_parsedMessage = (Parse_Header: #PM_header, -decodeheader);
						#PM_parsedMessage -> insert: 'body' = #PM_body;
					/if;
					
			
			/if;
					__POPmill_close__: #PM_pName, #PM_connectWithin;


		/if;
		

			
		if: #PM_parsedMessage->type == map;
			return: #PM_parsedMessage;
		else: #PM_pDontDecode;
			return: #PM_message;
		else;
			return: #PM_header + '\r\n\r\n' + #PM_body;
		/if;	
		
	/Define_Tag;
	
	
// ********************************************************************************************

	// [POP_Addresses]
	// 11/17/2002 Converted For __POPmill__


	Define_tag: 'POP_Addresses';
	 
		local: 'PM_pHeader'       = (named_param: '-header');
		local: 'PM_pField'        = array;
		local: 'PM_rFinal'        = map;
		local: 'PM_tGetField'     = string;
		local: 'PM_tGetAddr'      = string;
		local: 'PM_tAryCurrAddr'  = array;
		local: 'PM_tCurrAddr'	  = string;
		local: 'PM_tCurrName'	  = string;
		local: 'PM_tCurrEmail'	  = string;
		local: 'PM_curFieldNo'	  = 0;
		local: 'PM_pName'		  = '';
		local: 'PM_connectWithin' = '';
		local: 'PM_parsedHeader'  = '';
		local: 'PM_pID'			  = '';


		local: 'PM_pField' = (named_param: '-field');
		
		if: #PM_pField->type == 'string';
			#PM_pField = (array: #PM_pField);
		/if;
		
		
		if: #PM_pHeader == '';
			library: $__POPmill_tcl__;
			if: #PM_connection->'PM_errorMessage' == 'No error';
				library: $__POPmill_udl__;
			/if;
		else;
				library: $__POPmill_rtl__;
		/if;
				
		fail_if: #PM_pField->size == 0, '2', 'The -Field parameter was not used or defined in [POP_Addresses]';
		fail_if: (#PM_pID == 'false' || #PM_pID == '') && #PM_pHeader == '', '2', 'You must either use -id, -uid or -header to extract addresses with [POP_Addresses].';

		loop: #PM_pField->size;
			handle: #PM_pField->get:loop_count == '' || (#PM_pField->get:loop_count != 'to' &&
													 #PM_pField->get:loop_count != 'from' && 
													 #PM_pField->get:loop_count != 'cc' && 
													 #PM_pField->get:loop_count != 'reply-to');
				if: #PM_pHeader == '';
					__POPmill_close__: #PM_pName, #PM_connectWithin;
				/if;
			/handle;
	
	

			loop: #PM_pField->size;
				fail_if: #PM_pField->get:loop_count != 'to' &&
						 #PM_pField->get:loop_count != 'from' &&
						 #PM_pField->get:loop_count != 'cc' &&
						 #PM_pField->get:loop_count != 'reply-to',
					'2',
						'The -Field parameter contains an invalid value in [POP_Addresses]';
			/loop;
		/loop;
			
						
			if: #PM_pHeader == '';
				if: #PM_connection->'PM_errorMessage' == 'No error';
					#PM_pHeader = #PM_connection->sendCommand: ('TOP ' + #PM_pID + ' 0'), true, -encodeNone;
				/if;
			/if;
	
			if: #PM_pHeader->type != map;
				#PM_parsedHeader = (parse_header: #PM_pHeader, -decodeheader);
			else;
				#PM_parsedHeader = #PM_pHeader;
			/if;
						
			if: #PM_pField->size == 1;
				
				#PM_rFinal	  = array;
				#PM_tGetField = (#PM_parsedHeader->find:(#PM_pField->get:1));
				
				if: #PM_tGetField != '';
				
					 #PM_tGetAddr  = (#PM_tGetField->split:'>,');
								
					loop: #PM_tGetAddr->size;
						#PM_tCurrAddr = #PM_tGetAddr->get:loop_count;
						#PM_tCurrAddr = (string_replaceregexp: -find='(^\\s|\\s$)', -replace='', #PM_tCurrAddr);
						#PM_tAryCurrAddr = (string_findregexp: -find='\\s<.+@.+\\..+', #PM_tCurrAddr);
											
						if: #PM_tAryCurrAddr->size == 1;
							#PM_tCurrName  = (string_removetrailing: -pattern = ((string_findregexp: -find='\\s<.+@.+\\..+', #PM_tCurrAddr)->get:1), #PM_tCurrAddr);
							#PM_tCurrEmail = (string_replaceregexp: -find = '(\\s|<|>)', -replace = '', #PM_tAryCurrAddr->get:1);						
							#PM_rFinal->insert: (pair: #PM_tCurrName = #PM_tCurrEmail);
						else;
							#PM_tCurrAddr = (string_replaceregexp: -find='<|>', -replace='', #PM_tCurrAddr);
							#PM_rFinal->insert: #PM_tCurrAddr;
						/if;
					/loop;
				/if;
					
			else: #PM_pField->size > 1;
					
				#PM_rFinal = map;	
						
				loop: #PM_pField->size;
				
					#PM_tGetField = (#PM_parsedHeader->find:(#PM_pField->get:loop_count));
					
					if: #PM_tGetField != '';
						#PM_rFinal->insert: (#PM_pField->get:loop_count) = (array);
					
					
					#PM_tGetAddr 	 = (#PM_tGetField->split:'>,');
					#PM_curFieldNo   = loop_count;	
					#PM_tAryCurrAddr = array;	
													
					loop: #PM_tGetAddr->size;
						#PM_tCurrAddr    = #PM_tGetAddr->get:loop_count;
						#PM_tCurrAddr    = (string_replaceregexp: -find='(^\\s|\\s$)', -replace='', #PM_tCurrAddr);
						#PM_tAryCurrAddr = (string_findregexp: -find='\\s<.+@.+\\..+', #PM_tCurrAddr);
						
						if: #PM_tAryCurrAddr->size == 1;
							#PM_tCurrName = (string_removetrailing: -pattern = ((string_findregexp: -find='\\s<.+@.+\\..+', #PM_tCurrAddr)->get:1), #PM_tCurrAddr);
							#PM_tCurrEmail = (string_replaceregexp: -find = '(\\s|<|>)', -replace = '', #PM_tAryCurrAddr->get:1);						
							#PM_rFinal->(find: #PM_pField->get:#PM_curFieldNo)->insert:(pair: #PM_tCurrName = #PM_tCurrEmail);
						else;
							#PM_tCurrAddr = (string_replaceregexp: -find='<|>', -replace='', #PM_tCurrAddr);
							#PM_rFinal->(find: #PM_pField->get:#PM_curFieldNo)->(insert: #PM_tCurrAddr);
						/if;
					/loop;
				
				/if;

			/loop;
				
				__POPmill_close__: #PM_pName, #PM_connectWithin;

			/if;
			
			
			return: #PM_rFinal;
	
	/Define_tag;


// ********************************************************************************************


// [POP_Delete]
// 11/17/2002 Converted For __POPmill__


	Define_Tag: 'POP_Delete';
		
	library: $__POPmill_tcl__;
	local: 'PM_messageCount' = 0;
	local: 'PM_marked' = false;
	local: 'PM_tErr' = '';
	
	loop: params->size;
		if: (params->get:loop_count) == '-marked';
			#PM_marked = true;	
			loop_abort;
		/if;
	/loop;

	if: #PM_connection->'PM_errorMessage' == 'No error';
		
		library: $__POPmill_udl__;
		Fail_If: #PM_pID == '', 2,  'The -id or -uid parameters were not used or defined in [POP_Delete]';
		
	
			#PM_messageCount = #PM_connection->'PM_messageCount';	// get message count
		
			if: #PM_pID <= #PM_messageCount;	
				#PM_tErr = #PM_connection->sendCommand: ('DELE ' + #PM_pID), false;				

				if: #PM_marked;
					if: #PM_tErr->BeginsWith: '-ERR';
						return: false;
					else;
						return: true;
					/if;
				/if;
				
			/if;		
			
			__POPmill_close__: #PM_pName, #PM_connectWithin;

		/if;	
		
		
	/Define_Tag;


// ********************************************************************************************


// [POP_Undelete]
// 11/17/2002 Converted For __POPmill__


	Define_Tag: 'POP_Undelete';
	
		library: $__POPmill_tcl__;
	    
		if: #PM_connection->'PM_errorMessage' == 'No error';
			#PM_connection->sendCommand: 'RSET', false;				
		/if;
		
	/Define_Tag;
	


	
	
// ********************************************************************************************


	// [POP_Command]
	// 11/14/2002 Converted For __POPmill__
	
	Define_Tag: 'POP_Command';

		library: $__POPmill_tcl__;
		
		local: 'PM_rFinal' = '';
		local: 'PM_pMultiline' = false;
		local: 'PM_pCommand' = '';


		loop: params -> size;
			if: params -> get: loop_count == '-multiline';
				#PM_pMultiline = true;
			/if;
			
			if: params->get:loop_count != '-multiline' && params->get:loop_count->type != 'pair' && params->get:loop_count->beginswith: '-' == false;
				#PM_pCommand = params->get:loop_count;
			/if;
			
		/loop;
						
		Fail_if: #PM_pCommand == '', '2', 'The command is missing in [POP_Command]';
				
			
		
		#PM_rFinal=@#PM_connection->sendCommand: #PM_pCommand, #PM_pMultiline, -encodeNone;
			
		if: #PM_pMultiline == '\r\n.\r\n';
			#PM_rFinal = (String_removeTrailing: -Pattern='\r\n.\r\n', #PM_rFinal);
		/if;
			
		__POPmill_close__: #PM_pName, #PM_connectWithin;

		return: #PM_rFinal;
		
	/Define_Tag;
		
?>