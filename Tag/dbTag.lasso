<?LassoScript

Define_tag: 'POPmill_BuildDatabase';	
	
auth_admin;

	inline: -database='site',
			-SQL = 'CREATE DATABASE popmill';
			
			if: (error_currenterror) != 'No error';
				local: 'error' = (error_currenterror);
			/if;
				
	/inline;

	inline: -database='site',
-SQL= 'CREATE TABLE IF NOT EXISTS popmill.host 
(
id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
username VARCHAR(255) NOT NULL,
server VARCHAR(64) NOT NULL)';

if: (error_currenterror) != 'No error';
	local: 'error' = (error_currenterror);
/if;

/inline;
			   
inline: -database='site',
-SQL= 'CREATE TABLE IF NOT EXISTS popmill.uid 
(
id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT, 
host_id INT UNSIGNED NOT NULL, 
uid VARCHAR(70) NOT NULL, 
INDEX (uid, host_id)
)';

if: (error_currenterror) != 'No error';
	local: 'error' = (error_currenterror);
/if;
/inline;


_datasource_reload: 1;

	inline: -database = 'lasso_internal',
			-table = 'security_datasource_databases',
			-operator = 'eq',
			'name'	  = 'popmill',
			-search;
		
			var: 'id' = (field: 'id');
			
			if: (error_currenterror) != 'No error';
				local: 'error' = (error_currenterror);
			/if;
		
		
	/inline;

	
	inline: -database = 'lasso_internal',
			-table 	  = 'security_datasource_databases',
			'alias' = 'popmill',
			'password' = '',
			'enabled' = 'Y',
			'always_use_defaults' = 'N',
			'extra' = '',
			'sql_stop' = '',
			-KeyField = 'ID',
			-KeyValue = (var: 'id'),
			-update;
			
	if: (error_currenterror) != 'No error';
		local: 'error' = (error_currenterror);
	/if;
	
	/inline;
	
_datasource_reload: 1;


if: (local_defined: 'error') == false;
	
	if: 
	return: 'The POPmill database has been successfully built.';
else;
	return: ('There was a problem building the database: ' + (var: 'error'));
/if;



/Define_Tag;


?>
