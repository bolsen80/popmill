<?LassoScript

// [Convert_DateToLocal] b5
//
// Brian Olsen
// POPmill v. 1.0

// History:
//
// 8/31/2002 - fixed bug with dates less than 10 when they appeared with no 0
// 10/17/2002 - Removed -day code and replaced it with better code
// 10/28/2002 - Added the -gmt option
// 11/12/2002 - fixed Lasso 6 [date] bug
// 11/22/2002 - added error check if string is not right
// 12/4/2002  - added error check for param->size
//			  - fixed problem where [date] was not reading the day right
// 12/22/2002 - converted all input into string type
// 			  - added a change to workaround a inane problem where BW decided to change
//				the damn behavior of the [date] tag ...
// 1/4/2003   - fixed problem with + time zones
//			  - fixed problem with -gmt
//			  - checks lasso_version better

define_tag: 'Convert_DateToLocal';
	// values from user
	
	local: 'day' = false,
		   'dateToConvert' = '',
		   'ary_date' = array,
		   'or_userDate' = string,
		   'lassoVersion' = (integer: (string_replace: -find='.', -replace='', (lasso_version: -lassoversion))),
		  'final_date' = '';

	if: #lassoVersion < 100;
		#lassoVersion = (integer: ((string: #lassoVersion) + '0'));
	/if;

	if: params->size != 0;
		loop: params->size;
			if: (params->get:loop_count) == '-day';
				local: 'day' = true;
			/if;
		/loop;
		
		loop: params->size;
		
			if: (params->get:loop_count->type != pair) || (params->get:loop_count) != '-day';
				local: 'dateToConvert' = (string: (params->get:loop_count));	
				local: 'ary_date' = #dateToConvert->split: ' ';
				#dateToConvert = (string_replace: -find=',', -replace='', #dateToConvert);
				
				if: #ary_date->size != 6;
					return: #ary_date->size;
				/if;
				
				if: (integer: (#ary_date->get:2)) < 10 && (string: (#ary_date->get:2))->length == 1;
					
					
					(#ary_date->get:2) = ('0' + (string: #ary_date->get:2));
													
					loop: #ary_date->size;
						#dateToConvert = (#dateToConvert + (string: #ary_date ->get:loop_count) + ' ');	
					/loop;
									
				/if;
				
				loop_abort;
			/if;
	
		/loop;
	

	#or_userDate=(date: #dateToConvert, -dateformat = '%a %d %b %Y %H:%M:%S %z');
	local: 'or_day' = (string_replace: -find=',', -replace='', #ary_date->get:1);

	
	// values to test time zone
	
	local: 'ser_timeZone' = (named_param: '-gmt');
		
	if: #ser_timeZone == '';
		local: 'ser_timeZone' = (string: (date_getlocaltimezone));
		
		if: #lassoVersion >= 601 && #ser_timeZone->length == 4;
			#ser_timeZone = '+' + #ser_timeZone;
		/if;
		
	/if;
	
		
	local: 'or_TimeZone' = (string: #ary_date->get:6);	
	// if the time zones equal each other, return value sent
	if: #ser_timeZone == #or_TimeZone;
		if: #day;	
			local: 'final_date_array' = array;
			#final_date_array->insert:#or_day;
			#final_date = (date: #dateToConvert, -dateformat = '%a %d %b %Y %H:%M:%S %z');
		 	#final_date = (string_removetrailing: -pattern=' GMT', #final_date);
			#final_date_array->insert:#final_date;
			return: #final_date_array;
		else;
			#final_date = (date: #dateToConvert, -dateformat = '%a %d %b %Y %H:%M:%S %z');
		 	#final_date = (string_removetrailing: -pattern=' GMT', #final_date);
			return: #final_date;

		/if;
	/if;
	
	
	// if the time zones don't equal each other
	if: #ser_timeZone->beginswith: '-';
		local: 'ser_timeZone' = (string_replace: -find='-', -replace='', #ser_TimeZone);
		local: 'ser_timeZone' = ('-' + #ser_timeZone);
	else;
		local: 'ser_timeZone' = (string_replace: -find='-', -replace='', #ser_TimeZone);
	/if;
			
	local: 'ser_sign' = (#ser_timeZone->substring: 1, 1);
	local: 'ser_hour' = (#ser_timeZone->substring: 2, 2);
	local: 'ser_min' = (#ser_timeZone->substring: 4, 2);
	local: 'ser_whole' = (integer: (#ser_timeZone->substring: 2, 4));
	local: 'ser_whole' = (string_replace: -find='-', -replace='', #ser_whole);
	
	
	if: #or_timeZone->beginswith: '-';
		local: 'or_timeZone' = (string_replace: -find='-', -replace='', #or_TimeZone);
		local: 'or_timeZone' = ('-' + #or_timeZone);
	else;
		local: 'or_timeZone' = (string_replace: -find='-', -replace='', #or_TimeZone);
	/if;
	
	
	local: 'or_sign' = (#or_TimeZone->substring: 1, 1);
	local: 'or_hour' = (#or_TimeZone->substring: 2, 2);
	local: 'or_min' = (#or_TimeZone->substring: 4, 2);
	local: 'or_whole' = (integer: (#or_timeZone->substring: 2, 4));

	
	
	local: 'or_timestamp' = (date: #dateToConvert, -dateformat = '%a %d %b %Y %H:%M:%S %z');
	local: 'final_date' = '',
		   'function' = '';
	
	if: #lassoVersion >= 601;
		if: #or_sign == '+';
			local: 'or_timestamp' = (date_add: #or_timestamp, -hour=(integer: #or_hour), -minute=(integer: #or_min));
		else: #or_sign == '-';
			local: 'or_timestamp' = (date_subtract: #or_timestamp, -hour=(integer: #or_hour), -minute=(integer: #or_min));
		/if;
	/if;
	
		
	if: #or_timestamp->type == 'null';
		return;
	/if;
	
	// convert time to local time
	
	local: 'hour' = 0,
		   'minute' = 0;
		
	if: #ser_sign == '+' && #or_sign == '+';
		
		
		if: #or_whole > #ser_whole;
					
			local: 'zoneDif' = (integer: #or_whole) - (integer: #ser_whole);
			
			if: #zoneDif >= 1000;
				local: 'hour' = (integer: ((string: #zoneDif)->substring: 1,2));	
				local: 'minute' = (integer: ((string: #zoneDif)->substring: 3,2));
			
			else: #zoneDif < 1000;
					
				local: 'hour' = (integer: ((string: #zoneDif)->substring: 1,1));
				local: 'minute' = (integer: ((string: #zoneDif)->substring: 2,2)); 
								
			/if; 
						
			#final_date = (date_subtract: #or_timestamp, -hour=#hour, -minute=#ser_min);
			#function = '-';
			
		else: #or_whole < #ser_whole;
		
			local: 'zoneDif' = (integer: #ser_whole) - (integer: #or_whole);
						
			if: #zoneDif >= 1000;
				local: 'hour' = (integer: ((string: #zoneDif)->substring: 1,2));	
				local: 'minute' = (integer: ((string: #zoneDif)->substring: 3,2));
			
			else: #zoneDif < 1000;
					
				local: 'hour' = (integer: ((string: #zoneDif)->substring: 1,1));
				local: 'minute' = (integer: ((string: #zoneDif)->substring: 2,2)); 
								
			/if; 			
			
			#final_date = (date_add: #or_timestamp, -hour=#hour, -minute=#ser_min);
			#function = '+';
		
		/if;
			
		
	else: #ser_sign == '-' && #or_sign == '-';

		if: #or_whole > #ser_whole;
			local: 'zoneDif' = (integer: #or_whole) - (integer: #ser_whole);

			if: #zoneDif >= 1000;
				local: 'hour' = (integer: ((string: #zoneDif)->substring: 1,2));	
				local: 'minute' = (integer: ((string: #zoneDif)->substring: 3,2));
			
			else: #zoneDif < 1000;
					
				local: 'hour' = (integer: ((string: #zoneDif)->substring: 1,1));
				local: 'minute' = (integer: ((string: #zoneDif)->substring: 2,2)); 
								
			/if; 
			

			
			#final_date = (date_add: #or_timestamp, -hour=#hour, -minute=#ser_min);
			
			#function = '+';
		
						
		else: #or_whole < #ser_whole;
			local: 'zoneDif' = (integer: #ser_whole) - (integer: #or_whole);
						
		if: #zoneDif >= 1000;
				local: 'hour' = (integer: ((string: #zoneDif)->substring: 1,2));	
				local: 'minute' = (integer: ((string: #zoneDif)->substring: 3,2));
			
			else: #zoneDif < 1000;
					
				local: 'hour' = (integer: ((string: #zoneDif)->substring: 1,1));
				local: 'minute' = (integer: ((string: #zoneDif)->substring: 2,2)); 
								
			/if; 
			
			
			
			#final_date = (date_subtract: #or_timestamp, -hour=#hour, -minute=#ser_min);
			#function = '-';
		
		/if;
	
	else: #ser_sign == '+' && #or_sign == '-';
	
		local: 'zoneDif' = (integer: #or_whole) + (integer: #ser_whole);
		
		if: #zoneDif >= 1000;
			
				local: 'hour' = (integer: ((string: #zoneDif)->substring: 1,2));	
				local: 'minute' = (integer: ((string: #zoneDif)->substring: 3,2));
			
		else: #zoneDif < 1000;
					
			local: 'hour' = (integer: ((string: #zoneDif)->substring: 1,1));
			local: 'minute' = (integer: ((string: #zoneDif)->substring: 2,2)); 
								
		/if; 		
				
		#final_date = (date_add: #or_timestamp, -hour=#hour, -minute=#ser_min);	
		#function = '+';
	
	else: #ser_sign == '-' && #or_sign == '+';
		
		local: 'zoneDif' = (integer: #or_whole) + (integer: #ser_whole);
			if: #zoneDif >= 1000;
				local: 'hour' = (integer: ((string: #zoneDif)->substring: 1,2));	
				local: 'minute' = (integer: ((string: #zoneDif)->substring: 3,2));
			
			else: #zoneDif < 1000;
					
				local: 'hour' = (integer: ((string: #zoneDif)->substring: 1,1));
				local: 'minute' = (integer: ((string: #zoneDif)->substring: 2,2)); 
								
			/if; 
			
		
		#final_date = (date_subtract: #or_timestamp, -hour=#hour, -minute=#ser_min);
		#function = '-';
			
	/if;	
	
	 #final_date = (string_removetrailing: -pattern=' GMT', #final_date);

	
	// Adding the day

	if: #day;
	
		local: 'days' = (array: 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
		local: 'old_year' = (date_getyear: #or_userDate);
		local: 'new_year' = (date_getyear: #final_date);
		local: 'old_month' = (date_getmonth: #or_userDate);
		local: 'new_month' = (date_getmonth: #final_date);
		local: 'old_day' = (date_getday: #or_userDate);
		local: 'new_day' = (date_getday: #final_date);
		local: 'add_day' = false;
		local: 'sub_day' = false;
		local: 'same_day' = true;
		local: 'final_day' = '';
		local: 'curr_loopCount' = 0;
		
		if: #old_year > #new_year;
			#sub_day = true;
			#same_day = false;
		else: #old_year < #new_year;
			#add_day = true;
			#same_day = false;
		else: #old_year == #new_year;
			#same_day=true;
		/if;
		
		if: #same_day=true;
			if: #old_month > #new_month;
				#sub_day = true;
				#same_day = false;
			else: #old_month < #new_month;
				#add_day = true;
				#same_day = false;
			else: #old_month == #new_month;
				#same_day = true;
			/if;
		/if;
		
		if: #same_day=true;
			if: #old_day > #new_day;
				#sub_day = true;
				#same_day = false;
			else: #old_day < #new_day;
				#add_day = true;
				#same_day = false;
			else: #old_day == #new_day;
				#same_day = true;
			/if;
		/if;
			
		
		if: #same_day;
			local: 'final_day' = #or_day;
		else: #add_day;
			loop: #days->size;
				if: #or_day == #days->get:loop_count;
					#curr_loopCount = loop_count;
				/if;
			/loop;
			
			if: #curr_loopCount == 7;
				#final_day = #days->get:1;

			else;
				#final_day = #days->get:(#curr_loopCount + 1);
			/if;
			
		else: #sub_day;
			loop: #days->size;
				if: #or_day == #days->get:loop_count;
					#curr_loopCount = loop_count;
				/if;
			/loop;
			
			if: #curr_loopCount == 1;
				#final_day = #days->get:7;
			else;
				#final_day = #days->get:(#curr_loopCount - 1);
			/if;
		/if;
		
		
		
		// concatenates with final date
			local: 'final_date_array' = array;
			#final_date_array->insert:#final_day;
			#final_date_array->insert:#final_date;
			return: #final_date_array;
		
	else;
		return: #final_date;
	/if;
	
	else;
		return;
	/if;


/define_tag;

?>