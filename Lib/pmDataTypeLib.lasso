<?LassoScript



	/* [__POPmill__]
		Data Type that logs and retains connections to the POP3 server.
		It extends to the [net] data type.
		
		Version History:
		
		11/11/2002 - Started code
				   - got it to work with all the POP3 commands
				   
	*/
	
	Define_Type: '__POPmill__', 'net';
	
		local: 'PM_errorMessage'   = 'No error',
			   'PM_errorCode'      = 0,
			   'PM_errorAlert'     = '',
			   'PM_popOpen'        = false,
			   'PM_stat'		   = '',
			   'PM_uidl'		   = array,
			   'PM_list'		   = array,
			   'PM_openConnection' = false,
			   'PM_serverGreeting' = '',
			   'PM_hostName'	   = '',
			   'PM_userName'	   = '',
			   'PM_messageCount'   = 0,
			   'PM_allMessageSize' = 0,
			   'PM_messageSizes'   = array;
			   
		
		define_tag: 'openConn';
		
			local: 'PM_pHost'       = @(params->(find:'-host')->(get:1)->second);
			local: 'PM_pUsername'   = @(params->(find:'-user')->(get:1)->second);
			local: 'PM_pPassword'   = @(params->(find:'-pass')->(get:1)->second);
			local: 'PM_pPort'	    = @(params->(find:'-port')->(get:1)->second);
			local: 'PM_pTimeout'    = @(params->(find:'-time')->(get:1)->second);
			local: 'PM_pAPOP'	    = @(params->(find:'-apop')->(get:1)->second);
			local: 'PM_pPOPOpen'	= @(params->(find:'-pope')->(get:1)->second);
			// local: 'PM_pNoUIDs'		= @(params->(find:'-nouids')->(get:1)->second);
			// local: 'PM_pNoMsgSize'	= @(params->(find:'-nomsgsize')->(get:1)->second);

			local: 'PM_timestamp'   = '';
			local: 'PM_md5pass'	    = '';
			local: 'PM_login'	    = '';
			local: 'PM_iGreeting'   = @(self->'PM_serverGreeting');
			local: 'PM_iErrorMsg'   = @(self->'PM_errorMessage');
			local: 'PM_iErrorCode'  = @(self->'PM_errorCode');
			local: 'PM_iErrorAlert' = @(self->'PM_errorAlert');
			local: 'PM_iOpenConn'   = @(self->'PM_openConnection');
			local: 'PM_iPOPOpen'	= @(self->'PM_popOpen');
			local: 'PM_iMsgSize'    = @(self->'PM_messageSizes');
			local: 'PM_tMsgSizeBuf' = array;
			local: 'PM_iUserName'	= @(self->'PM_userName');
			local: 'PM_iHostName'	= @(self->'PM_hostName');
			local: 'PM_iList'		= @(self->'PM_list');
			local: 'PM_iStat'	    = @(self->'PM_stat');
			local: 'PM_iAllMsgSize' = @(self->'PM_allMessageSize');
			local: 'PM_iMsgCount'	= @(self->'PM_messageCount');
			local: 'PM_tUidBuf'		= array;
			local: 'PM_iUid'		= @(self->'PM_uidl');
			local: 'PM_tUidBufCurr' = '';
			
			protect;
				if: (self->parent->connect: #PM_pHost, #PM_pPort) == 0;

					if: (error_currenterror) == 'No error';
											
						if: #PM_pAPOP;
							// -apop called
							
							#PM_iGreeting = self->parent->read: 200;
							#PM_timestamp = (string_findRegExp: #PM_iGreeting, -find='<.+>');
							
							if: #PM_timestamp->size == 0;
								
								// could not find <PID@domain.tld>
							
								#PM_iErrorMsg  = 'Cannot login with APOP';
								#PM_iErrorCode = 3;
								#PM_login = '';
								self->parent->write: 'QUIT\r\n';
							else; 
							
								// found <PID@domain.tld>
								
								#PM_md5pass = (encrypt_md5: ((#PM_timestamp->get:1) + #PM_pPassword));
								
								self->parent->write: ('APOP ' + #PM_pUsername + ' ' + #PM_md5pass + '\r\n');
								
								#PM_iGreeting = self->parent->read:200;
								#PM_login = self->parent->read: 200;
								#PM_login = self->parent->read: 200;


							/if;
							
						else;
							// -apop not called, use USER, PASS
							
							self->parent->write: ('USER ' + #PM_pUsername + '\r\n');
							self->parent->write: ('PASS ' + #PM_pPassword + '\r\n');
							
							
							#PM_iGreeting = self->parent->read:200;
							#PM_login = self->parent->read: 200;
							
						/if;
						
						
						if: #PM_login->BeginsWith: '-ERR' && #PM_login != '';
						
							#PM_iErrorMsg  = 'Unable to login';
							#PM_iErrorCode = 1; 
							self->parent->write: 'QUIT\r\n';
						
						else: #PM_login->BeginsWith: '+OK';
						// No error
							
							#PM_iOpenConn = true;
							#PM_iUserName = (params->(find:'-user')->(get:1)->second);
							#PM_iHostName = (params->(find:'-host')->(get:1)->second);
														
							if: #PM_pPOPOpen; // if user used [POP_Open]
								#PM_iPOPOpen = true; 
							/if;
						
							
							// get unique-id
						
								self->parent->write: 'UIDL\r\n';
								while: (self->parent->wait:20000, NET_WAIT_READ) == NET_WAIT_READ;
								#PM_tUidBuf += (self->parent->read: 10000);
									
									if: #PM_tUidBuf->endswith: '\r\n.\r\n';
										loop_abort;
									/if;
								/while;
								
								#PM_tUidBuf= #PM_tUidBuf->split: '\r\n';
								#PM_tUidBuf->remove;
								#PM_tUidBuf->remove;
								#PM_tUidBuf->remove: 1;
	
								
								// fix in case connection/auth information overlaps with command output
							
								if: #PM_tUidBuf->get:1 >> '+OK';
									#PM_tUidBuf->remove: 1;
								/if;							
							
								// removes number from uid output - commented out because it might not be needed
							
								//	loop: #PM_tUidBuf->size;
								//		#PM_iUid->insert:(#PM_tUidBuf->get:loop_count->split:' '->get:2);
								//	/loop;
							
								#PM_iUid = #PM_tUidBuf;
							
						
							// get STAT
								
							self->parent->write: 'STAT\r\n';
							#PM_iStat = (self->parent->read: 200);
							#PM_iStat->trim;
							#PM_iAllMsgSize = #PM_iStat->split: ' '->get:3;
							#PM_iMsgCount   = #PM_iStat->split: ' '->get:2;
						
							// get message sizes
							
							
								self->parent->write: 'LIST\r\n';
								while: (self->parent->wait:20000, NET_WAIT_READ) == NET_WAIT_READ;
									#PM_tMsgSizeBuf += (self->parent->read: 10000);
									
									if: #PM_tMsgSizeBuf->endswith: '\r\n.\r\n';
										loop_abort;
									/if;
								/while;
							
			
								#PM_tMsgSizeBuf = #PM_tMsgSizeBuf->split:'\r\n';

								#PM_tMsgSizeBuf->remove;
								#PM_tMsgSizeBuf->remove;							
								#PM_tMsgSizeBuf->remove:1;
								#PM_iList = #PM_tMsgSizeBuf;
		
								loop: #PM_iList->size;
									#PM_iMsgSize->insert: (#PM_iList->get:loop_count->split: ' '->get: 2);
								/loop;
							
													
						/if;
						
					else;
						
						// error if POP3 connection does not exist.	
						 
						#PM_iErrorMsg  = 'POP3 connection does not exist';
						#PM_iErrorCode = 2; 
						
					/if;
				/if;
		
			/protect;	
			
		/define_tag;
		
		define_tag: 'closeConn';
			
			local: 'PM_iErrorMsg'   = @(self->'PM_errorMessage');
			local: 'PM_iErrorCode'  = @(self->'PM_errorCode');
			local: 'PM_iErrorAlert' = @(self->'PM_errorAlert');
			local: 'PM_iOpenConn'   = @(self->'PM_openConnection');
			
			
			if: #PM_iErrorMsg == 'No error';
				self->parent->write: 'QUIT\r\n';
				self->parent->close;
			/if;
			
		/define_tag;
		
		
		define_tag: 'sendCommand';
				
			local: 'PM_iErrorMsg' = @(self->'PM_errorMessage');
			local: 'PM_pComm' 	  = @(params->get:1);
			local: 'PM_pMLine' 	  = @(params->get:2);
			local: 'PM_rFinal' 	  = '';
			local: 'PM_iMsgSize'  = @(self->'PM_messageSizes');
			local: 'PM_msgSize'   = 0;
						
			if: #PM_iErrorMsg == 'No error';
			
				if: #PM_pMLine == false;
					self->parent->write: (#PM_pComm + '\r\n');
					#PM_rFinal = self->parent->read: 200;
				else;
					
				
					if: #PM_pComm->BeginsWith: 'RETR';
						
						
						// not being used
						// #PM_msgSize = #PM_iMsgSize->get:(integer: #PM_pComm->split: ' '->get:2);
						self->parent->write: (#PM_pComm + '\r\n');
						while: (self->parent->wait:20000, NET_WAIT_READ) == NET_WAIT_READ;
							#PM_rFinal += (self->parent->read: 100000, -encodenone);
							
							if: #PM_rFinal->endswith: '\r\n.\r\n';
								loop_abort;
							/if;
						/while;
						
						#PM_rFinal = (string_replaceregexp: -find='^\\+OK.+\\r\\n', -replace='', #PM_rFinal);
						#PM_rFinal = (string_replaceregexp: -find='^\\+OK\\s*(.|\\d)+', -replace='', #PM_rFinal);
						
						
					else;

						self->parent->write: (#PM_pComm + '\r\n');
						while: (self->parent->wait:20000, NET_WAIT_READ) == NET_WAIT_READ;
							#PM_rFinal += (self->parent->read: 1000000);
							
							if: #PM_rFinal->endswith: '\r\n.\r\n';
								loop_abort;
							/if;
						/while;
						
						#PM_rFinal = (string_replaceregexp: -find='^\\+OK.+\\r\\n', -replace='', #PM_rFinal);
						#PM_rFinal = (string_replaceregexp: -find='^\\+OK\\s*(.|\\d)+', -replace='', #PM_rFinal);

						
					/if;
				/if;
			/if;
			return: #PM_rFinal;
			
			
		/define_tag;
	
	
	/Define_Type;
	
?>