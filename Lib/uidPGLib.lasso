<?LassoScript

	local: 'PM_pUID' 				= (named_param: '-uid');
	local: 'PM_pID'  				= (named_param: '-id');
	local: 'PM_pMID' 				= (named_param: '-mid');
	local: 'PM_aryNewUID' 			= (self -> 'PM_aryNewUID');
	local: 'PM_aryUniqueID' 		= (self -> 'PM_aryUniqueID');
	local: 'PM_aryDownloadSpecific' = (self->'PM_aryDownloadSpecific');

	
	if: #PM_pUID != '';
	
			if: #PM_aryNewUID->size != 0;
			
				loop: #PM_aryNewUID->size;
				
					if: #PM_aryNewUID->get:loop_count == #PM_pUID;
						#PM_pID = loop_count;
						loop_abort;
					/if;
				/loop;
				
			else;
			
			
				loop: #PM_aryUniqueID->size;
			
					if: #PM_aryUniqueID->get:loop_count->split:' '->get:2 == #PM_pUID;
							#PM_pID = #PM_aryUniqueID->get:loop_count->split:' '->get:1;
							loop_abort;
					/if;
					
				/loop;
			
			/if;
			
		if: #PM_pID == '';
			#PM_pID = 'false';
		/if;
		
	else: #PM_pMID != '';
	
		loop: #PM_aryDownloadSpecific->size;
		
			if: #PM_pMID == #PM_aryDownloadSpecific->get:loop_count;
				#PM_pID = (loop_count);
				loop_abort;
			/if;
		
		/loop;
		
		if: #PM_pID == '';
			#PM_pID = 'false';
		/if;
	
	/if;
	
?>


