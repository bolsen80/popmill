<?LassoScript

	// Unique ID
	
	local: 'PM_pUID' 		= (named_param: '-uid');
	local: 'PM_pID' 		= (named_param: '-id');
	
	if: (var_defined: '__popmill_connectname__');
	
		local: 'PM_aryUniqueID' = @#PM_connection->'PM_uidl';
	
		 if: #PM_pUID != '';
			
			loop: #PM_aryUniqueID->size;
			
				if: #PM_aryUniqueID->get:loop_count->split: ' '->get:2 == #PM_pUID;
						#PM_pID = (integer: #PM_aryUniqueID->get:loop_count->split:' '->get:1);
						loop_abort;
				/if;
			
			/Loop;
			
			if: #PM_pID == '';
				#PM_pID = 'false';
			/if;
	
	
		/if;
	
	/if;
	
?>