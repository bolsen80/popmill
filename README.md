# POPmill

## Introduction

I am posting the source of my old shareware library for a little posterity. I could possibly set it up to run it, but I am not that interested. I hope though it could be useful for other people.

### What is this

POPmill was a shareware project I worked on in my early 20s (like 2001-2003). I started this because my brother and I were working on something that would involve sending and then checking emails, using Lasso 5+. In retrospect, since the deploy target was OS X, using a program like `fetchmail` would have been a simpler thing if I had the pressure to get something done. Also, using IMAP would have been better at the time.

I also included the documentation, which I wrote with AppleWorks.

Some of the comments in some source files were inside jokes. I would be a more professional with source code comments now, but I think it was sort of the spirit of how it was with little easter eggs in Mac OS in the day. ... And their still funny though :)

### Design

I was sort of new at programming and had mighty high ideals about 'correct code'. The first versions contained a simple [POPGet] "custom type" (read: class) that would represent a POP3 connection end. But, I decided that simpler functions made sense for the API. In retrospect, they were odd decisions I made, but whatever.

## Some attributions

There is one piece of code that I include written by someone else, which is the quoted-printable code. I give that attribution in the documentation.

## Docs

I include also a PDF build of the documentation plus the original AppleWorks source files.
